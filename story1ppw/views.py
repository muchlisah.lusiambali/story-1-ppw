from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"index.html")

def description(desc):
    return render(desc, "description.html")

def background(education):
    return render(education, "background.html")

def socialmedia(socmed):
    return render(socmed, "socialmedia.html")